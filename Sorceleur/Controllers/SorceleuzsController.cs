﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sorceleur.Models;

namespace Sorceleur.Controllers
{
    public class SorceleuzsController : Controller
    {
        private readonly MonContext _context;

        public SorceleuzsController(MonContext context)
        {
            _context = context;
        }

        // GET: Sorceleuzs
        public async Task<IActionResult> Index()
        {
            Bags();
            return View(await _context.Sorceleuzs.AsNoTracking().ToListAsync());
        }

        // GET: Sorceleuzs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            Bags();
            if (id == null)
            {
                return NotFound();
            }

            var sorceleuz = await _context.Sorceleuzs
                .FirstOrDefaultAsync(m => m.Id == id);
            if (sorceleuz == null)
            {
                return NotFound();
            }

            return View(sorceleuz);
        }

        // GET: Sorceleuzs/Create
        public IActionResult Create()
        {
            Bags();
            return View();
        }

        // POST: Sorceleuzs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,nom,prenom,avatar,raceSorceleur")] Sorceleuz sorceleuz, IFormFile MonAvatar)
        {
            DateTime thisDay = DateTime.Today;
            if (ModelState.IsValid)
            {
                if (MonAvatar.FileName.EndsWith(".jpg") || MonAvatar.FileName.EndsWith(".png"))
                {
                    // Remplir strin avater par le nv chemin avec une date 
                    sorceleuz.avatar = MonAvatar.FileName.Insert(0, thisDay.ToString("yyyyMMddHHmmss"));
                    // Creer le chemin du notre avatar sur le serveur
                    // Chemin avec une date pour ne pas ecraser les donnees
                    string chemin = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "Avatars", MonAvatar.FileName.Insert(0,thisDay.ToString("yyyyMMddHHmmss")));
                    // Creation du fichier au memoire
                    var File = new FileStream(chemin, FileMode.Create);
                    // Creation D'une Copies  vers le serveur
                    MonAvatar.CopyToAsync(File);
                    _context.Add(sorceleuz);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
            Bags();
            return View(sorceleuz);
        }

        // GET: Sorceleuzs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            Bags();
            if (id == null)
            {
                return NotFound();
            }

            var sorceleuz = await _context.Sorceleuzs.FindAsync(id);
            if (sorceleuz == null)
            {
                return NotFound();
            }
            return View(sorceleuz);
        }

        // POST: Sorceleuzs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,nom,prenom,avatar,raceSorceleur")] Sorceleuz sorceleuz, IFormFile MonAvatar1)
        {
            Bags();
            if (id != sorceleuz.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    
                    /////////////////////////////////////////
                    var local = _context.Set<Sorceleuz>()
                        .Local
                        .FirstOrDefault(entry => entry.Id.Equals(id));

                    // check if local is not null 
                    if (local != null)
                    {
                        // detach
                        _context.Entry(local).State = EntityState.Detached;
                    }
                    // set Modified flag in your entry
                    _context.Entry(sorceleuz).State = EntityState.Modified;
                    // save 
                    _context.SaveChanges();
                    /*
                     * 
                     * 
                     *  _context.Update(sorceleuz);
                        await _context.SaveChangesAsync();
                     *
                     * 
                     * */
                    //SRC // https://stackoverflow.com/questions/36856073/the-instance-of-entity-type-cannot-be-tracked-because-another-instance-of-this-t
                    //SRC // https://entityframeworkcore.com/knowledge-base/48117961/the-instance-of-entity-type-----cannot-be-tracked-because-another-instance-of-this-type-with-the-same-key-is-already-being-tracked
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SorceleuzExists(sorceleuz.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(sorceleuz);
        }

        // GET: Sorceleuzs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            Bags();
            if (id == null)
            {
                return NotFound();
            }

            var sorceleuz = await _context.Sorceleuzs
                .FirstOrDefaultAsync(m => m.Id == id);
            if (sorceleuz == null)
            {
                return NotFound();
            }

            return View(sorceleuz);
        }

        // POST: Sorceleuzs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            Bags();
            var sorceleuz = await _context.Sorceleuzs.FindAsync(id);
            _context.Sorceleuzs.Remove(sorceleuz);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SorceleuzExists(int id)
        {
            Bags();
            return _context.Sorceleuzs.AsNoTracking().Any(e => e.Id == id);
        }
        public void Bags()
        {
            string A = _context.Sorceleuzs.ToList().Count().ToString();
            ViewBag.A = A;
            string B = _context.Quetes.ToList().Count().ToString();
            ViewBag.B = B;
            string C = _context.Monstres.ToList().Count().ToString();
            ViewBag.C = C;
            string D = _context.Quetes.Sum(s => s.recompense).ToString();
            ViewBag.D = D;
        }
    }
}
