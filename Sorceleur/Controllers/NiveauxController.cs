﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sorceleur.Models;

namespace Sorceleur.Controllers
{
    public class NiveauxController : Controller
    {
        private readonly MonContext _context;

        public NiveauxController(MonContext context)
        {
            _context = context;
        }

        // GET: Niveaux
        public async Task<IActionResult> Index()
        {
            Bags();
            var monContext = _context.Niveaux.Include(n => n.monstre);
            return View(await monContext.AsNoTracking().ToListAsync());
        }

        // GET: Niveaux/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            Bags();
            if (id == null)
            {
                return NotFound();
            }

            var niveau = await _context.Niveaux
                .Include(n => n.monstre)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (niveau == null)
            {
                return NotFound();
            }

            return View(niveau);
        }

        // GET: Niveaux/Create
        public IActionResult Create()
        {
            Bags();
            ViewData["MonstreId"] = new SelectList(_context.Monstres, "Id", "nom");
            return View();
        }

        // POST: Niveaux/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,niveau,point,MonstreId")] Niveau niveau)
        {
            Bags();
            if (ModelState.IsValid)
            {
                _context.Add(niveau);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["MonstreId"] = new SelectList(_context.Monstres, "Id", "nom", niveau.MonstreId);
            return View(niveau);
        }

        // GET: Niveaux/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            Bags();
            if (id == null)
            {
                return NotFound();
            }

            var niveau = await _context.Niveaux.FindAsync(id);
            if (niveau == null)
            {
                return NotFound();
            }
            ViewData["MonstreId"] = new SelectList(_context.Monstres, "Id", "nom", niveau.MonstreId);
            return View(niveau);
        }

        // POST: Niveaux/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,niveau,point,MonstreId")] Niveau niveau)
        {
            Bags();
            if (id != niveau.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(niveau);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NiveauExists(niveau.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["MonstreId"] = new SelectList(_context.Monstres, "Id", "nom", niveau.MonstreId);
            return View(niveau);
        }

        // GET: Niveaux/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            Bags();
            if (id == null)
            {
                return NotFound();
            }

            var niveau = await _context.Niveaux
                .Include(n => n.monstre)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (niveau == null)
            {
                return NotFound();
            }

            return View(niveau);
        }

        // POST: Niveaux/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            Bags();
            var niveau = await _context.Niveaux.FindAsync(id);
            _context.Niveaux.Remove(niveau);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool NiveauExists(int id)
        {
            Bags();
            return _context.Niveaux.Any(e => e.Id == id);
        }
        public void Bags()
        {
            string A = _context.Sorceleuzs.ToList().Count().ToString();
            ViewBag.A = A;
            string B = _context.Quetes.ToList().Count().ToString();
            ViewBag.B = B;
            string C = _context.Monstres.ToList().Count().ToString();
            ViewBag.C = C;
            string D = _context.Quetes.Sum(s => s.recompense).ToString();
            ViewBag.D = D;
        }
    }
}
