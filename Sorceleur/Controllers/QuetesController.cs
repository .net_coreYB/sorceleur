﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sorceleur.Models;

namespace Sorceleur.Controllers
{
    public class QuetesController : Controller
    {
        private readonly MonContext _context;

        public QuetesController(MonContext context)
        {
            _context = context;
        }

        // GET: Quetes
        public async Task<IActionResult> Index()
        {
            Bags();
            var monContext = _context.Quetes.Include(q => q.sorceleur);
            return View(await monContext.AsNoTracking().ToListAsync());
        }

        // GET: Quetes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            Bags();
            if (id == null)
            {
                return NotFound();
            }

            var quete = await _context.Quetes
                .Include(q => q.sorceleur)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (quete == null)
            {
                return NotFound();
            }

            return View(quete);
        }

        // GET: Quetes/Create
        public IActionResult Create()
        {
            Bags();
            ViewData["SorceleurId"] = new SelectList(_context.Sorceleuzs, "Id", "nom");
            return View();
        }

        // POST: Quetes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,recompense,dificultee,endroit,SorceleurId")] Quete quete)
        {
            Bags();
            if (ModelState.IsValid)
            {
                _context.Add(quete);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["SorceleurId"] = new SelectList(_context.Sorceleuzs, "Id", "nom", quete.SorceleurId);
            return View(quete);
        }

        // GET: Quetes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            Bags();
            if (id == null)
            {
                return NotFound();
            }

            var quete = await _context.Quetes.FindAsync(id);
            if (quete == null)
            {
                return NotFound();
            }
            ViewData["SorceleurId"] = new SelectList(_context.Sorceleuzs, "Id", "nom", quete.SorceleurId);
            return View(quete);
        }

        // POST: Quetes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,recompense,dificultee,endroit,SorceleurId")] Quete quete)
        {
            Bags();
            if (id != quete.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    /////////////////////////////////////////
                    var local = _context.Set<Quete>()
                        .Local
                        .FirstOrDefault(entry => entry.Id.Equals(id));

                    // check if local is not null  
                    if (local != null)
                    {
                        // detach
                        _context.Entry(local).State = EntityState.Detached;
                    }
                    // set Modified flag in your entry
                    _context.Entry(quete).State = EntityState.Modified;

                    // save 
                    _context.SaveChanges();
                    /*
                     * 
                     * 
                     *  _context.Update(quete);
                     await _context.SaveChangesAsync();
                     *
                     * 
                     * */
                    //SRC // https://stackoverflow.com/questions/36856073/the-instance-of-entity-type-cannot-be-tracked-because-another-instance-of-this-t
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!QueteExists(quete.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["SorceleurId"] = new SelectList(_context.Sorceleuzs, "Id", "nom", quete.SorceleurId);
            return View(quete);
        }

        // GET: Quetes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            Bags();
            if (id == null)
            {
                return NotFound();
            }

            var quete = await _context.Quetes
                .Include(q => q.sorceleur)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (quete == null)
            {
                return NotFound();
            }

            return View(quete);
        }

        // POST: Quetes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            Bags();
            var quete = await _context.Quetes.FindAsync(id);
            _context.Quetes.Remove(quete);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool QueteExists(int id)
        {
            Bags();
            return _context.Quetes.AsNoTracking().Any(e => e.Id == id);
        }
        public void Bags()
        {
            string A = _context.Sorceleuzs.ToList().Count().ToString();
            ViewBag.A = A;
            string B = _context.Quetes.ToList().Count().ToString();
            ViewBag.B = B;
            string C = _context.Monstres.ToList().Count().ToString();
            ViewBag.C = C;
            string D = _context.Quetes.Sum(s => s.recompense).ToString();
            ViewBag.D = D;
        }
    }
}
