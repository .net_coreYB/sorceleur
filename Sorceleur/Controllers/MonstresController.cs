﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sorceleur.Models;

namespace Sorceleur.Controllers
{
    public class MonstresController : Controller
    {
        private readonly MonContext _context;

        public MonstresController(MonContext context)
        {
            _context = context;
        }

        // GET: Monstres
        public async Task<IActionResult> Index()
        {
            Bags();

            return View(await _context.Monstres.AsNoTracking().ToListAsync());
        }

        // GET: Monstres/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            Bags();
            if (id == null)
            {
                return NotFound();
            }

            var monstre = await _context.Monstres
                .FirstOrDefaultAsync(m => m.Id == id);
            if (monstre == null)
            {
                return NotFound();
            }

            return View(monstre);
        }

        // GET: Monstres/Create
        public IActionResult Create()
        {
            Bags();
            return View();
        }

        // POST: Monstres/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,nom,poids,raceMonstre")] Monstre monstre)
        {
            Bags();
            if (ModelState.IsValid)
            {
                _context.Add(monstre);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(monstre);
        }

        // GET: Monstres/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            Bags();
            if (id == null)
            {
                return NotFound();
            }

            var monstre = await _context.Monstres.FindAsync(id);
            if (monstre == null)
            {
                return NotFound();
            }
            return View(monstre);
        }

        // POST: Monstres/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("Id,nom,poids,raceMonstre")] Monstre monstre)
        {
            Bags();
            if (id != monstre.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    /////////////////////////////////////////
                    var local = _context.Set<Monstre>()
                        .Local
                        .FirstOrDefault(entry => entry.Id.Equals(id));

                    // check if local is not null 
                    if (local != null)
                    {
                        // detach
                        _context.Entry(local).State = EntityState.Detached;
                    }
                    // set Modified flag in your entry
                    _context.Entry(monstre).State = EntityState.Modified;

                    // save 
                    _context.SaveChanges();
                    /*
                     * 
                     * 
                     *  _context.Update(monstre);
                        await _context.SaveChangesAsync();
                     *
                     * 
                     * */
                    //SRC // https://stackoverflow.com/questions/36856073/the-instance-of-entity-type-cannot-be-tracked-because-another-instance-of-this-t
                    //SRC // https://entityframeworkcore.com/knowledge-base/48117961/the-instance-of-entity-type-----cannot-be-tracked-because-another-instance-of-this-type-with-the-same-key-is-already-being-tracked
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MonstreExists(monstre.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(monstre);
        }

        // GET: Monstres/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            Bags();
            if (id == null)
            {
                return NotFound();
            }

            var monstre = await _context.Monstres
                .FirstOrDefaultAsync(m => m.Id == id);
            if (monstre == null)
            {
                return NotFound();
            }

            return View(monstre);
        }

        // POST: Monstres/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            Bags();
            var monstre = await _context.Monstres.FindAsync(id);
            _context.Monstres.Remove(monstre);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MonstreExists(int id)
        {
            Bags();
            return _context.Monstres.Any(e => e.Id == id);
        }
        public void Bags()
        {
            string A = _context.Sorceleuzs.ToList().Count().ToString();
            ViewBag.A = A;
            string B = _context.Quetes.ToList().Count().ToString();
            ViewBag.B = B;
            string C = _context.Monstres.ToList().Count().ToString();
            ViewBag.C = C;
            string D = _context.Quetes.Sum(s => s.recompense).ToString();
            ViewBag.D = D;
        }
    }
}
