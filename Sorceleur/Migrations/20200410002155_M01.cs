﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sorceleur.Migrations
{
    public partial class M01 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Monstres",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nom = table.Column<string>(nullable: false),
                    poids = table.Column<string>(nullable: true),
                    raceMonstre = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Monstres", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sorceleuzs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nom = table.Column<string>(nullable: false),
                    prenom = table.Column<string>(nullable: false),
                    avatar = table.Column<string>(nullable: true),
                    raceSorceleur = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sorceleuzs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Niveaux",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    niveau = table.Column<int>(nullable: false),
                    point = table.Column<int>(nullable: false),
                    MonstreId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Niveaux", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Niveaux_Monstres_MonstreId",
                        column: x => x.MonstreId,
                        principalTable: "Monstres",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Quetes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    recompense = table.Column<int>(nullable: false),
                    dificultee = table.Column<string>(nullable: true),
                    endroit = table.Column<string>(nullable: true),
                    SorceleurId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Quetes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Quetes_Sorceleuzs_SorceleurId",
                        column: x => x.SorceleurId,
                        principalTable: "Sorceleuzs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Vaincres",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nbr = table.Column<int>(nullable: false),
                    QueteId = table.Column<int>(nullable: false),
                    MonstreId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vaincres", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Vaincres_Monstres_MonstreId",
                        column: x => x.MonstreId,
                        principalTable: "Monstres",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Vaincres_Quetes_QueteId",
                        column: x => x.QueteId,
                        principalTable: "Quetes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Niveaux_MonstreId",
                table: "Niveaux",
                column: "MonstreId");

            migrationBuilder.CreateIndex(
                name: "IX_Quetes_SorceleurId",
                table: "Quetes",
                column: "SorceleurId");

            migrationBuilder.CreateIndex(
                name: "IX_Vaincres_MonstreId",
                table: "Vaincres",
                column: "MonstreId");

            migrationBuilder.CreateIndex(
                name: "IX_Vaincres_QueteId",
                table: "Vaincres",
                column: "QueteId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Niveaux");

            migrationBuilder.DropTable(
                name: "Vaincres");

            migrationBuilder.DropTable(
                name: "Monstres");

            migrationBuilder.DropTable(
                name: "Quetes");

            migrationBuilder.DropTable(
                name: "Sorceleuzs");
        }
    }
}
