﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sorceleur.Models
{
    public class MonContext : DbContext
    {
        public DbSet<Sorceleuz> Sorceleuzs { get; set; }
        public DbSet<Quete> Quetes { get; set; }
        public DbSet<Monstre> Monstres { get; set; }
        public DbSet<Vaincre> Vaincres { get; set; }
        public DbSet<Niveau> Niveaux { get; set; }
        public MonContext() : base(SqlServerDbContextOptionsExtensions.UseSqlServer(new DbContextOptionsBuilder(), @"Data Source=.\SQLEXPRESS;initial catalog=Sorceleur;integrated security=true;").Options)
        {

        }

    }
}
