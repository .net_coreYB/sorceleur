﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sorceleur.Models
{
    public class Vaincre
    {
        public int Id { get; set; }
        public int nbr { get; set; }
        public Quete quete { get; set; }
        public int QueteId { get; set; }
        public Monstre monstre { get; set; }
        public int MonstreId { get; set; }
    }
}
