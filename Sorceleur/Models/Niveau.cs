﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Sorceleur.Models
{
    public class Niveau
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Entre 1 et 5")]
        [Range(1, 5)]
        public int niveau { get; set; }
        [Required(ErrorMessage = "Entre 10 et 50")]
        [Range(10, 50)]
        public int point { get; set; }
        public Monstre monstre { get; set; }
        public int MonstreId { get; set; }

    }
}
