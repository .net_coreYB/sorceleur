﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Sorceleur.Models
{
    public class Sorceleuz
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Votre nom est obligatoire")]
        public string nom { get; set; }
        [Required(ErrorMessage = "Votre Prenom est obligatoire")]
        public string prenom { get; set; }
        public string avatar { get; set; }
        public string raceSorceleur { get; set; }
        public IList<Quete> Quetes { get; set; }
    }
}
