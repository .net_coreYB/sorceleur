﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Sorceleur.Models
{
    public class Quete
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Recompense obligatoire")]
        [DataType(DataType.Currency)]
        public int recompense { get; set; }
        public string dificultee { get; set; }
        public string endroit { get; set; }
        public Sorceleuz sorceleur { get; set; }
        public int SorceleurId { get; set; }
        public IList<Vaincre> vaincres { get; set; }
    }
}
