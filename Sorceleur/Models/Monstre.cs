﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Sorceleur.Models
{
    public class Monstre
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Monstre nom est obligatoire")]
        public string nom { get; set; }
        public string poids { get; set; }
        public string raceMonstre { get; set; }
        public IList<Vaincre> vaincres { get; set; }
        public IList<Niveau> niveaux { get; set; }
    }
}
